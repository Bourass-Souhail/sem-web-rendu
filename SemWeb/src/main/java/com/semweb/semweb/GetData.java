package com.semweb.semweb;

import okhttp3.*;

public class GetData {

    public static void main(String[] args) throws Exception {

        GetData http = new GetData();

        System.out.println("\nSend Http GET request");

        String ressource = "events-0046090b-b440-449d-86d5-456683a90894/";

        http.get(ressource);

    }

    // GET request
    private void get(String ressource) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/turtle");
        Request request = new Request.Builder()
                .url("https://territoire.emse.fr/ldp/loic-container/" + ressource)
                .method("GET", null)
                .addHeader("Slug", "loic-data")
                .addHeader("Content-Type", "text/turtle")
                .addHeader("Authorization", "Basic bGRwdXNlcjpMaW5rZWREYXRhSXNHcmVhdA==")
                .build();
        Response response = client.newCall(request).execute();
        String data = response.body().string();
        System.out.println(data);
    }

}
