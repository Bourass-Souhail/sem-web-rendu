package com.semweb.semweb;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.component.CalendarComponent;
import net.fortuna.ical4j.model.component.VEvent;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Calendar {

    public static ArrayList parse() throws IOException, ParserException {

        // Read iCalendar file
        ArrayList<String> EventList = new ArrayList<String>();

        InputStream in = new FileInputStream("C:\\COURS\\Semantic_Web\\semweb\\semweb\\src\\main\\"
                + "java\\com\\semweb\\semweb\\ADECal.ics");

        CalendarBuilder builder = new CalendarBuilder();

        net.fortuna.ical4j.model.Calendar calendar = builder.build(in);

        // Create RDF model
        Model model = ModelFactory.createDefaultModel();

        // Iterate over events in calendar
        for (CalendarComponent event : calendar.getComponents(VEvent.VEVENT)) {
            // Convert event to RDF and add to model
            Event.convertEvent((VEvent) event, model);
        }
        // We need this for temporary string holder
        StringWriter stringWriter = new StringWriter();

        // we write the event in turtle format
        model.write(stringWriter,"Turtle");

        String temp = stringWriter.toString();

        EventList = new ArrayList<>(Arrays.asList(temp.split("\n\n")));

        return EventList;

    }

    public static Model parseModel() throws IOException, ParserException {

        // Read iCalendar file
        InputStream in = new FileInputStream("C:\\COURS\\Semantic_Web\\semweb\\semweb\\src\\main\\"
                + "java\\com\\semweb\\semweb\\ADECal.ics");

        CalendarBuilder builder = new CalendarBuilder();

        net.fortuna.ical4j.model.Calendar calendar = builder.build(in);

        // Create RDF model
        Model model = ModelFactory.createDefaultModel();

        // Iterate over events in calendar
        for (CalendarComponent event : calendar.getComponents(VEvent.VEVENT)) {
            // Convert event to RDF and add to model
            Event.convertEvent((VEvent) event, model);
        }

        return model;


    }

    public static void main(String[] args) throws Exception {

        /*ArrayList<String> EventList = new ArrayList<String>();

        EventList = parse();*/

        Model test = parseModel();

        StringWriter stringWriter = new StringWriter();

        // Serialize RDF model to RDF/XML
        test.write(stringWriter, "Turtle");

        String rdf = stringWriter.toString();
        System.out.print(rdf);

        OutputStream out = new FileOutputStream("output-model.ttl");
        test.write(out, "Turtle");

    }


}
