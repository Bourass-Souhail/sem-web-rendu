package com.semweb.semweb;

import okhttp3.*;

public class DeleteData {

    public static void main(String[] args) throws Exception {

        DeleteData http = new DeleteData();

        System.out.println("\nSend Http DELETE request");

        String ressource = "";

        http.delete(ressource);

    }
    // DELETE request
    private void delete(String ressource) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/turtle");
        RequestBody body = RequestBody.create("\r\n", mediaType);
        Request request = new Request.Builder()
                .url("https://territoire.emse.fr/ldp/loic-container/" + ressource)
                .method("DELETE", body)
                .addHeader("Slug", "loic-data")
                .addHeader("Content-Type", "text/turtle")
                .addHeader("Authorization", "Basic bGRwdXNlcjpMaW5rZWREYXRhSXNHcmVhdA==")
                .build();
        Response response = client.newCall(request).execute();
    }

}
