package com.semweb.semweb;

import org.apache.jena.rdf.model.*;

public class Util {
    public void getRawData(String triples) {
        Model model = ModelFactory.createDefaultModel();
        //RDFDataMgr.read(model, new StringReader(triples), null, "TURTLE");
        StmtIterator iterator = model.listStatements();
        while (iterator.hasNext()) {
            Statement statement = iterator.next();
            RDFNode object = statement.getObject();
            System.out.println(object.toString());
        }
    }
}
