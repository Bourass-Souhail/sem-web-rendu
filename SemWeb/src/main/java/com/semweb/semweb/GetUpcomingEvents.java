package com.semweb.semweb;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;

public class GetUpcomingEvents {
    public static void main(String[] args) {
        // Load the turtle file into a Jena Model
        Model model = ModelFactory.createDefaultModel();
        model.read("C:\\COURS\\Semantic_Web\\semweb\\semweb\\output-model.ttl", "TURTLE");

        // Define the SPARQL query
        String queryString =
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> " +
                        "PREFIX event: <http://example.org/event/> " +
                        "SELECT ?event ?startTime " +
                        "WHERE { " +
                        "?event a event:Event . " +
                        "?event event:startTime ?startTime . " +
                        "FILTER(?startTime >= '2023-01-06T08:00:00Z'^^xsd:dateTime) " +
                        "} " +
                        "ORDER BY ?startTime";

        // Execute the query
        Query query = QueryFactory.create(queryString);
        try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
            ResultSet results = qexec.execSelect();

            // Print the results
            while (results.hasNext()) {
                QuerySolution solution = results.next();
                Resource event = solution.getResource("event");
                Literal startTime = solution.getLiteral("startTime");
                System.out.println(event + " starts at " + startTime);
            }
        }
    }
}
