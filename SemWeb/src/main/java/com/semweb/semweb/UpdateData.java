package com.semweb.semweb;

import okhttp3.*;

public class UpdateData {

    public static void main(String[] args) throws Exception {

        UpdateData http = new UpdateData();

        System.out.println("\nSend Http UPDATE request");

        String ressource = "events-05b3c822-850d-4e8a-82f3-79fdc4641ede/";

        String updateString = "<events-05b3c822-850d-4e8a-82f3-79fdc4641ede>" +
                " <http://schema.org/hasAttended> true .";

        http.update(ressource, updateString);

    }

    // UPDATE request
    private void update(String ressource, String updateString) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/turtle");
        RequestBody body = RequestBody.create(updateString, mediaType);
        Request request = new Request.Builder()
                .url("https://territoire.emse.fr/ldp/loic-container/" + ressource)
                .patch(body)
                .addHeader("Content-Type", "application/sparql-update")
                .addHeader("Authorization", "Basic bGRwdXNlcjpMaW5rZWREYXRhSXNHcmVhdA==")
                .build();
        Response response = client.newCall(request).execute();
    }
}
