package com.semweb.semweb;

import net.fortuna.ical4j.model.component.VEvent;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.util.UUID;

public class Event {

    public static void convertEvent(VEvent event, Model model) {
        // Generate unique event URI
        Resource eventUri = model.createResource("events/" + UUID.randomUUID().toString());

        // Add event properties to model
        eventUri.addProperty(RDF.type, model.createResource("http://schema.org/Event"));
        eventUri.addProperty(model.createProperty("http://schema.org/name"), event.getSummary().getValue());
        eventUri.addProperty(model.createProperty("http://schema.org/startDate"), model.createTypedLiteral(event.getStartDate().getDate(), XSDDatatype.XSDdate));
        eventUri.addProperty(model.createProperty("http://schema.org/endDate"), model.createTypedLiteral(event.getEndDate().getDate(), XSDDatatype.XSDdate));
        eventUri.addProperty(model.createProperty("http://schema.org/location"), event.getLocation().getValue());
    }
}
