package com.semweb.semweb;

import okhttp3.*;

import java.util.ArrayList;

public class PostData {

    public static void main(String[] args) throws Exception {

        PostData http = new PostData();

        System.out.println("\nSend Http POST request");

        ArrayList list = Calendar.parse();

        for (Object data : list) {
            http.sendPost(data.toString());
            System.out.println(data.toString());
        }


        /*String container = "@prefix ldp: <http://www.w3.org/ns/ldp#>.\r\n<loic-container> a ldp:Container.\r\n";
        http.sendPost(container);*/
    }

    //POST request
    private void sendPost(String ressource) throws Exception {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/turtle");
        RequestBody body = RequestBody.create(
                ressource, mediaType);
        Request request = new Request.Builder()
                .url("https://territoire.emse.fr/ldp/loic-container/")
                .method("POST", body)
                .addHeader("Slug", "loic-data")
                .addHeader("Content-Type", "text/turtle")
                .addHeader("Authorization", "Basic bGRwdXNlcjpMaW5rZWREYXRhSXNHcmVhdA==")
                .build();
        Response response = client.newCall(request).execute();
    }
}
